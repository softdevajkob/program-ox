/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptsh.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OXGame {

    static int count = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] board = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};

    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showBoard() {
        System.out.println(" 123");
        for (int row = 0; row < board.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < board[row].length; col++) {
                System.out.print(board[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            //  System.out.println("row :"+ row + " col: " + col);
            if (board[row][col] == '-') {
                board[row][col] = player;
                count++;
                break;
            }
            System.out.println("Error: board at row and col is not empty!!");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (board[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (board[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkT() {
        if (board[0][0] == 'X') {
            if (board[1][1] == 'X') {
                if (board[2][2] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }

        }
        if (board[0][2] == 'X') {
            if (board[1][1] == 'X') {
                if (board[2][0] == 'X') {
                    isFinish = true;
                    winner = player;
                }
            }

        }
        if (board[0][0] == 'O') {
            if (board[1][1] == 'O') {
                if (board[2][2] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }

        }
        if (board[0][2] == 'O') {
            if (board[1][1] == 'O') {
                if (board[2][0] == 'O') {
                    isFinish = true;
                    winner = player;
                }
            }

        }

    }

    static void checkDraw() {
        if (winner == '-' && count == 9) {
            isFinish = true;
            System.out.println("Play Draw!!");

        }

    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkT();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showBoard();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}
